Title: Hak 46: Hak yang Meminta
Date: 2024-01-01

    وحق السائل إعطاؤه على قدر حاجته. 

> Dan hak peminta (_sâ`il_),
>
> Berikanlah ia sesuai kadar kebutuhannya.