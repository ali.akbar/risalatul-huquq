Title: Hak 39: Hak Lawan yang Diklaim
Date: 2023-12-31

    حق الخصم تدعى عليه

    وحق خصمك الذي تدعي عليه إن كنت محقا في دعواك أجملت مقاولته، و لم تجحد حقه وإن كنت مبطلا في دعواك اتقيت الله عزوجل وتبت إليه وتركت الدعوى. 

> Dan hak lawan yang engkau klaim,
>
> Jika klaim dakwaanmu dibenarkan,
>   maka sopanlah dalam berucap padanya,
>   janganlah halangi haknya.
>
> Jika klaim dakwaanmu salah,
>   maka takutlah kepada Allah _azza wa jalla_,
>   bertobatlah kepada-Nya,
>   dan tariklah klaim itu.