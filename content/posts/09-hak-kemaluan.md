Title: Hak 09: Hak Kemaluan
Date: 2023-12-21

وَأَمَّا حَقُّ فَرْجِكَ انْ تُحْصِنَهُ عَن الْزِّنا و تَحْفِظَهَ مِن أنْ يُنْظَر الَيْهِ

> Dan hak kemaluanmu
>
> Kau melindunginya dari zina,
> dan menjaganya dari perhatian padanya