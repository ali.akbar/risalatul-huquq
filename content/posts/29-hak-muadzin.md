Title: Hak 29: Hak Muadzin
Date: 2023-12-31

    وَأمّا حَقُّ الْمُؤَذِّنِ فَأَنْ تَعْلَمَ أنّهُ مُذَكِّرُ لَكَ برَبكَ وداعٍ لك إلى حَظِّكَ وعَونُكَ على قضاء فرض الله عليك، فَاشْكُرْهُ عَلَى ذَلِكَ شُكْرَكَ لِلْمُحْسِنِ إلَيكَ. 

> Dan hak muadzin,
>
> Kau perlu tahu,
>   ia mengingatkanmu atas Tuhanmu,
>   memanggilmu kepada keberuntunganmu,
>   membantumu melaksanakan kewajiban Allah atasmu.
>
> Maka berterimakasihlah kepadanya atas kebaikannya pada dirimu.