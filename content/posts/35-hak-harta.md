Title: Hak 35: Hak Harta
Date: 2023-12-31

    وأما حق مالك فأن لا تأخذه إلا من حله، ولا تنفقه إلا في وجهه، ولا تؤثر به على نفسك من لا يحمدك، فاعمل فيه بطاعة ربك ولا تبخل به فتبوء بالحسرة و الندامة مع التبعة ولا قوة إلا بالله. 

> Dan hak kekayaanmu (mal),
>
> Kau tidak mengambilnya, kecuali dari yang halal,
> Tidak menafkahkannya, kecuali pada yang sesuai
> Jangan menggunakannya untuk meninggikan dirimu pada orang yang tak memujimu
>
> Maka beramallah dalam harta dengan ketaatan kepada Tuhanmu,
>   janganlah kikir dengannya,
>   karena kamu akan ditimpa penyesalan dan kekecewaan
>   saat menderita akibat buruknya.
>
> _wa lâ quwwata illâ billâh_