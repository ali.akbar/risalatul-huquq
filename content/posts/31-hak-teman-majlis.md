Title: Hak 31: Hak Teman Majlis
Date: 2023-12-31

    وأما حق جليسك فأن تلين له جانبك، وتنصفه في مجاراة اللفظ، ولا تقوم من مجلسك إلا بإذنه، ومن يجلس إليك يجوز له القيام عنك بغير إذنه، وتنسى زلاته وتحفظ خيراته، ولا تسمعه إلا خيرا. 

> Dan hak teman majlismu,
>
> Kau bersikap lemah lembut padanya,
>   memberikan kesempatan yang sama padanya
>   dalam beradu argumen,
>
> Janganlah bangkit dari majlis
>   kecuali dengan izin darinya,
>   namun boleh saja orang yang bersamamu pergi tanpa izin darinya.
>
> Lupakanlah kesalahannya, ingat hal baik darinya,
>   janganlah menyampaikan sesuatu tentangnya, kecuali kebaikan