Title: Hak 50: Hak Orang yang Sekepercayaan
Date: 2024-01-01

    وحق أهل ملتك إضمار السلامة لهم والرحمة لهم، والرفق بمسيئهم وتألفهم واستصلاحهم، وشكر محسنهم وكف الأذى عنهم، وتحب لهم ما تحب لنفسك، و تكره لهم ما تكره لنفسك، وأن تكون شيوخهم بمنزلة أبيك، وشبابهم بمنزلة اخوتك، وعجائزهم بمنزلة أمك، والصغار بالمنزلة أولادك. 

> Dan hak yang sekepercayaan denganmu (_ahlu millah_),
>
> Jamin keselamatan untuk mereka,
>   kasih sayang kepada mereka,
>   kelembutan terhadap keburukan mereka,
>   berteman dengan mereka,
>   usahakan kesejahteraan mereka.
>
> Berterimakasihlah kepada kebaikan mereka,
>   jauhkanlah bahaya dari mereka.
>   cintailah bagi mereka apa yang kau cintai bagi dirimu,
>   tidak sukailah bagi mereka apa yang kau tidak sukai bagi dirimu sendiri.
>
> Sesepuh mereka menempati tempat ayahmu,
>   pemuda mereka menempati tempat saudara kandungmu,
>   perempuan tua mereka menempati tempat ibumu,
>   dan yang kecil menempati tempat anak-anakmu.