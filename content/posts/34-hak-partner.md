Title: Hak 34: Hak Partner
Date: 2023-12-31

    وأما حق الصاحب فأن تصحبه بالتفضل والإنصاف، وتكرمه كما يكرمك ولا تدعه يسبق إلى مكرمة، فان سبق كافأته، وتودّه كما يودك، وتزجره عما يهم به من معصية، وكن عليه رحمة ولا تكن عليه عذابا ولا قوة إلا بالله. 

> Dan hak sohib,
>
> Kau bersahabat kepadanya dengan baik dan imbang,
>   menghormatinya seperti ia menghormatimu,
>   janganlah membiarkan ia yang pertama berlaku hormat,
>   jika ia yang duluan, maka balaslah.
>
> Berharaplah untuknya seperti ia berharap bagimu,
>   tahanlah ia dari maksiat yang hendak ia lakukan,
>   kasihanilah ia, jangan siksa dirinya.
>
> _wa lâ quwwata illâ billâh_