Title: Hak 40: Hak Peminta Masukan
Date: 2023-12-31

    وحق المستشير إن علمت له رأيا حسنا له رأيا أشرت عليه وإن لم تعلم أرشدته إلى من يعلم. 

> Dan hak peminta masukan (_mustasyîr_),
>
> Jika engkau tahu bahwa pandangannya benar,
>   sampaikan masukan padanya untuk mengikuti hal tersebut.
>
> Jika engkau tidak tahu,
>   maka sampaikan padanya untuk
>   meminta masukan pada yang mengetahuinya.