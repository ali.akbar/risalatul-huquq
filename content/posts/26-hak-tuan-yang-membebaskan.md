Title: Hak 26: Hak Tuan yang Membebaskanmu
Date: 2023-12-31

    وأما حق مولاك المنعم عليك فأن تعلم أنه أنفق فيك ماله وأخرجك من ذل الرق ووحشته إلى عز الحرية وانسها، فأطلقك من أسر الملكة، وفك عنك قيد العبودية، وأخرجك من السجن، وملكك نفسك، وفرغك لعبادة ربك وتعلم أنه أولى الخلق بك في حياتك وموتك، وأن نصرته عليك واجبة بنفسك، وما احتاج إليه منك، ولا قوة إلا بالله. 

> Dan hak tuanmu yang menyukaimu (dengan membabaskanmu dari perbudakan),
>
> Kau perlu tahu,
>   ia telah menafkahkan hartanya padamu,
>
> Ia mengeluarkanmu,
>   dari kehinaan dan kesepian kungkungan,
>   menuju kemuliaan dan kenyamanan kemerdekaan,
>
> Kemudian, ia telah membebaskanmu dari belenggu perbudakan,
>   melepaskan ikatan perbudakan darimu.
>
> Ia keluarkan dirimu dari penjara,
>   memberimu kepemilikan atas dirimu,
>   dan memberimu kebebasan beribadah kepada Tuhanmu,
>
> Ketahuilah,
>   sungguh ia ciptaan Tuhan yang paling dekat denganmu
>   pada hidupmu dan matimu,
>   membantunya adalah wajib bagi dirimu,
>   juga apa yang ia perlukan darimu
>
> _wa lâ quwwata illâ billâh_