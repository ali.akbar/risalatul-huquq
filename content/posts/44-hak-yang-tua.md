Title: Hak 44: Hak yang Tua
Date: 2024-01-01

    وحق الكبير توقيره لسنه، وإجلاله لتقدمه في الإسلام قبلك، وترك مقابلته عند الخصام، ولا تسبقه إلى طريق، ولا تتقدمه، ولا تستجهله وإن جهل عليك احتملته وأكرمته لحق الإسلام وحرمته.

> Dan hak orang yang lebih tua (_kabir_),
>
> Kau menghargainya karena umurnya,
>   dan memuliakannya karena ia telah islam sebelummu.
>
> Kau hindari berbantahan dengannya saat ada perselisihan,
>   janganlah mendahuluinya di jalan,
>   janganlah menjadikannya bodoh.
>
> Jika ia melakukan kebodohan (_jahil_) kepadamu,
>   abaikan itu, dan muliakan ia
>   karena hak islam dan kehormatannya