Title: Hak 25: Hak Saudara
Date: 2023-12-31

    وَأَمّا حَقُّ أَخِيكَ فَأن تَعْلَمَ أَنّهُ يَدُكَ وَعِزُّكَ وَقُوَّتُكَ ، فَلا تَتَّخِذْهُ سِلاحًا علَى مَعصيةِ اللَّهِ ولا عُدَّةً لِلظُّلْمِ لِخَلْقِ اللَّهِ، ولا تَدَعْ نُصْرتَهُ عَلَى عَدُوِّهِ، والنَّصِيحَة لَهُ فَاِنْ أطاعَ اللهَ وإلا فلْيَكُنِ الله أكرم عليك منه ولا قوة إلا بالله. 

> Dan hak saudara-(kandung)-mu,
>
> Kau perlu tahu,
>   ia adalah tanganmu, semangatmu dan kekuatanmu,
>
> Maka janganlah menggunakannya sebagai
>   senjata maksiat kepada Allah,
>   jangan pula sebagai alat untuk menzalimi ciptaan Allah.
>
> Janganlah membiarkannya,
>   tak membantunya atas musuhnya,
>   maupun memberinya nasihat.
>
> Jika ia taat kepada Allah, itu adalah baik.
>   jika tidak, maka Allah lebih mulia atasmu darinya.
>
> _wa lâ quwwata illâ billâh_