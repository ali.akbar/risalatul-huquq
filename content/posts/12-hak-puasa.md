Title: Hak 12: Hak Puasa
Date: 2023-12-26


    وَأَمَّا حَقُّ الصَّوْمِ فَأَنْ تَعْلَمَ أَنَّهُ حِجَابٌ ضَرَبَهُ اللهُ عَلَى لِسَانِكَ وَسَمْعِكَ وبَصَرِكَ وَفَرْجِكَ وبَطْنِكَ لِيَسْتركَ بهِ مِن النَّارِ. فان تركتَ الصومَ خرقتَ ستر الله عليك. 

> Dan hak puasa,
>
> Kau perlu tahu, ia adalah hijab yang dipasang Allah
>   atas lisanmu, pendengaranmu, penglihatanmu, kemaluanmu dan perutmu.
>   Dengannya, Ia lindungi kau dari api neraka.
> Jika kau tinggalkan puasa,
>   berarti kau telah robek perlindungan Allah atasmu.