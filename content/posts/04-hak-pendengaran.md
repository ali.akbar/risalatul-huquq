Title: Hak 04: Hak Pendengaran
Date: 2023-07-08

    وَأَمَّا حَقُّ السَّمْعِ فَتَنْزِيهُهُ عَنْ سَماع الغيبةِ و سَماعِها لا يحِلّ سَماعه.

> Dan hak pendengaran,
>
> Kau menjaganya bersih  
>   dari mendengarkan _ghibah_,
>   dan mendengarkan yang tidak halal untuk didengar