Title: Hak 49: Hak orang yang berbuat buruk
Date: 2024-01-01

    وحق من ساءك أن تعفو عنه وإن علمت أن العفو يضر انتصرت قال الله تبارك وتعالى "ولمن انتصر من بعد ظلمه فأولئك ما عليهم من سبيل" (الشورى/41). 

> Dan hak yang berbuat buruk padamu,
>
> Kau maafkan ia,
>   namun jika kau tahu maaf itu akan membahayakan,
>   maka pertahankahlah dirimu.
>
> Allah _tabâraka wa ta'âlâ_ berfirman:
>
> وَلَمَنِ انْتَصَرَ بَعْدَ ظُلْمِهٖ فَاُولٰۤىِٕكَ مَا عَلَيْهِمْ مِّنْ سَبِيْلٍۗ (QS. As-Syura:41)
>
> Akan tetapi, sungguh siapa yang membela diri setelah teraniaya, tidak ada satu alasan pun (untuk menyalahkan) mereka. (Terjemahan Qur'an Kemenag)