Title: Hak 38: Hak Lawan yang Mengklaim
Date: 2023-12-31

    وحق الخصم المدعي عليك، فإن كان ما يدعي عليك حقا كنت شاهده على نفسك، ولم تظلمه وأوفيته حقه، وإن كان ما يدعي به باطلا رفقت به ولم تأت في أمره غير الرفق، ولم تسخط ربك في أمره ولا قوة إلا بالله. 

> Dan hak lawan (_khashm_) yang mengklaim atasmu,
> 
> Jika klaimnya atasmu benar,
>   bersaksilah terhadap dirimu.
>   janganlah menzaliminya, dan penuhilah haknya.
>
> Jika klaimnya atasmu batil,
>   berlakulah kepadanya dengan halus,
>   janganlah kau tampilkan selain kehalusan;
>
> Janganlah kau buat Tuhanmu murka dalam urusan kepadanya,
>
> _wa lâ quwwata illâ billâh_