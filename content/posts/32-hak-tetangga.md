Title: Hak 32: Hak Tetangga
Date: 2023-12-31

    وأما حق جارك فحفظه غائبا وإكرامه شاهدا ونصرته إذا كان مظلوما، ولا تتبع له عورة، فإن علمت عليه سوءا سترته عليه، وإن علمت أنه يقبل نصيحتك نصحته فيما بينك وبينه، ولا تسلمه عند شديدة، وتقيل عثرته، وتغفر ذنبه، وَتُعَاشِرَهُ مُعَاشَرَةً كَرِيمَةً. وَلا قُوَّةَ إلا باللهِ.

> Dan hak tetanggamu,
>
> Kau jaga ia saat ia tidak ada,
>   memuliakannya saat ia hadir,
>   membantunya saat dia dizalimi.
>
> Janganlah mencari-cari auratnya (hal yang mempermalukannya),
>   jika kau tahu kejelekan atasnya, kau sembunyikan hal itu,
>   jika kau tahu ia akan menerima nasihatmu,
>   nasihatilah ia tentang apapun diantaramu dan dia,
>
> Jangan tinggalkan ia dalam kesulitan,
>   bebaskan ia dari kealpaannya, maafkanlah dosanya,
>   dan berinteraksilah dengan cara yang terhormat.
>
> _wa lâ quwwata illâ billâh_