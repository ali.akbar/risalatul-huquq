Title: Hak 22: Hak Ibu
Date: 2023-12-31

    و أما حَقُّ أُمِّكَ، فَأَنْ تَعْلَمَ أَنَّهَا حَمَلَتكَ حَيْثُ لا يحتمل أحدٌ أحداَ، وأعْطَتْكَ مِن ثَمْرَةِ قَلبِها ما لا يُعطي أحدٌ أحداَ، ووَقَتْكَ بِجَمِيعِ جَوارحِها، ولَمْ تبال أن تجوع وتطعمك، وتعطش وتسقيك، وتعرى وتكسوك، وتضحى وتظلك، وتهجر النوم لأجلك، ووَقَتْكَ الحَرَّ والبَردَ، لتكون لها، فانك لا تُطيق شُكرها إلّا بِعَونِ اللهِ وتَوفيقه. 

> Dan hak ibumu,
>
> Kau perlu tahu,
>   ia mengandungmu, membawamu ke tempat yang tiada seorangpun membawa orang lain,
>   ia memberimu makan buah dari jantungnya yang tidak ada seorangpun memberikan bagi orang lain,
>   dan ia melindungimu dengan seluruh organ tubuhnya.
> ia tak peduli
>   lapar asalkan memberimu makan,
>   haus asalkan memberimu minum,
>   telanjang asalkan memberimu baju,
>   terpapar matahari asalkan menaungimu,
> ia tinggalkan tidur untuk kepentinganmu,
>   melindungimu dari panas dan dingin,
>   agar kau ada untuknya.
> Sungguh,
>   kau tak bisa cukup berterimakasih padanya,
>   kecuali dengan bantuan dan taufik Allah