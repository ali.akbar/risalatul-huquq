Title: Hak 48: Hak yang melaluinya Allah membahagiakanmu
Date: 2024-01-01

    وحق من سرك الله تعالى به أن تحمد الله عزوجل أولا ثم تشكره.

> Dan hak orang yang dengannya Allah _ta'ala_ membuatmu bahagia,
>
> Pujilah Allah _azza wa jalla_,
>   kemudian sampaikanlah terimakasih kepadanya.