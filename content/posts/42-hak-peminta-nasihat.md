Title: Hak 42: Hak Peminta Nasihat
Date: 2023-12-31

    وحق المستنصح أن تؤدي إليه النصيحة، وليكن مذهبك الرحمة له والرفق به. 

> Dah hak peminta nasihat (_mustanshih_),
>
> Kau beri ia nasihat,
>   jadikanlah pandanganmu rahmat baginya,
>   dan berlaku ramahlah dengannya