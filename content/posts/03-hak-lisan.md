Title: Hak 03: Hak Lisan
Date: 2023-07-08

    وَأَمَّا حَقُّ اللّسَان فَإكْرَامُهُ عَنِ الْخَنَى وتَعْوِيدُهُ الْخَيْرِ وَ تَرك الْفُضُول الَّتي لا فائِدَةِ لَها و البِرُّ بالنَّاسِ و حُسْن القَولِ فيهم. 

> Dan hak lisan,
>
> Kau sadari ia terlalu mulia  
>   dari kata-kata yang vulgar,  
>   membiasakannya dengan kata-kata yang baik,
>   membebaskannya dari ikut campur hal-hal yang tak ada faedahnya,
>   sampaikan perhomatan bagi manusia,
>   dan perkataan yang baik terkait mereka