Title: Hak 11: Hak Haji
Date: 2023-12-26


    وَأَمَّا حَقُّ الحَجّ أَنْ تَعْلَمَ أَنّه وِفادةٌ إلى رَبّكَ، و فِرارٌ إليه من ذُنوبكَ و بهِ قبولُ تَوبتِكَ وقَضاءُ الفَرضِ الَّذي أوجَبَه الله عَليك.

> Dan hak haji,
>
> Kau perlu tahu, ia adalah serambi menuju Tuhanmu,
>   serta pelarianmu kepada-Nya dari dosamu.
> Melaluinya, taubatmu dikabulkan,
>   dan kau tunaikan kewajiban dari Allah atasmu