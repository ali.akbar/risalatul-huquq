Title: Hak 47: Hak yang Diminta
Date: 2024-01-01

    وحق المسؤول إن أعطى فاقبل منه بالشكر والمعرفة بفضلة، وإن منع فاقبل عذره.

> Dan hak yang diminta (_mas`ûl_),
>
> Jika ia memberi, maka terimalah
>   dengan rasa syukur dan akui kebajikannya.
>
> Jika ia tidak memberi, maka terimalah
>   alasannya.