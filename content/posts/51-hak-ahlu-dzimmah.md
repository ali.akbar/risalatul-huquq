Title: Hak 51: Hak _Ahlu Dzimmah_
Date: 2024-01-01

    وحق الذمة أن تقبل منهم ما قبل الله عز وجل [منهم] ولا تظلمهم ما وفوا لله عز وجل بعهده 

> Dan hak _dzimmah_ (non-muslim yang dilindungi di negara Islam),
>
> Kau terima dari mereka
    apa yang telah Allah _azza wa jalla_ terima (dari mereka),
>   dan janganlah menzalimi mereka
>   selama mereka penuhi perjanjian mereka dengan Allah _azza wa jalla_