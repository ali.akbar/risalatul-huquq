Title: Hak 41: Hak Pemberi Masukan
Date: 2023-12-31

    وحق المشير عليك أن لا تتهمه فيما لا يوافقك من رأيه وإن وافقك حمدت الله عز وجل.

> Dan hak orang yang kau mintai masukan (_musyîr_),
>
> Janganlah engkau menuduhnya
>   jika pandangannya tidak sesuai pandanganmu,
>
> Jika pandangannya sejalan,
>   pujilah Allah _azza wa jalla_
