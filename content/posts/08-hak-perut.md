Title: Hak 08: Hak Perut
Date: 2023-07-28

    وَأَمَّا حَقُّ بَطْنِكَ فَأَنْ لا تَجْعَلَهُ وِعَاءً لِلْحَرَامِ وَلا تَزيد على الشَّبْعَ. 

> Dan hak perutmu,
>
> Tidak kau jadikan ia tempat bagi yang haram,
> dan tidak menambah isinya sampai lebih dari kenyang 