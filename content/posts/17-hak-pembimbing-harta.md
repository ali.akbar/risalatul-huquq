Title: Hak 17: Hak tuanmu
Date: 2023-12-31

    وأمَّا حَقُّ سَائِسِكَ بالمِلْكِ فأن تطيعه ولا تعصيه إلا فيما يسخط الله عز وجل فانه لا طاعة لمخلوق في معصية الخالق.

> Dan hak pengendalimu (_sâ'is_) dengan kepemilikan (_misalnya tuan bagi budak, pen._),
>
> Kau mematuhinya dan tidak membangkangnya
>   kecuali pada hal yang Allah _azza wa jalla_ tidak berkenan,
> Karena tiada ketaatan kepada ciptaan
>   dalam kemaksiatan pada Sang Pencipta