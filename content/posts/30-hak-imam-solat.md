Title: Hak 30: Hak Imam Salat Fardhu
Date: 2023-12-31

    وحق إمامك في صلاتك فأن تعلم أنه تقلد السفارة فيما بينك وبين ربك عز وجل وتكلم عنك ولم تتكلم عنه ودعا لك ولم تدع له وكفاك هول المقام بين يدي الله عز وجل، فإن كان نقص كان به دونك، وإن كان تماما كنت شريكه، ولم يكن له عليك فضل، فوقى نفسك بنفسه وصلاتك بصلاته فتشكر له على قدر ذلك. 

> Dan hak imam pada salatmu,
>
> Kau perlu tahu,
>   ia menjadi penengah antara dirimu dan Tuhanmu _azza wa jalla_,
> Ia berucap untukmu, tapi engkau tidak mengucapkan apapun untuknya,
>   ia berdoa untukmu, tapi engkau tidak berdoa untuknya.
>   ia menghindarkanmu dari ketakutan berdiri dihadapan Allah _azza wa jalla_.
> 
> Jika kurang sempurna (salatnya),
>   maka itu hanya padanya, tidak padamu.
>
> Namun jika sempurna,
>   maka itu untukmu juga, tidak ada kelebihannya atas dirimu.
>
> Maka lindungilah dirimu melaluinya,
>   dan lindungi salatmu dengan salatnya,
>   serta berterimakasihlah padanya atas hal tersebut.