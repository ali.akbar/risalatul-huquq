Title: Hak 37: Hak Kolega 
Date: 2023-12-31

    وحق الخليط أن لا تغره ولا تغشه ولا تخدعه وتتقي الله تبارك وتعالى في أمره.

> Dan hak kolegamu (_khalit_),
>
> Janganlah kau mengecohnya,
>   berbohong padanya,
>   maupun menipunya.
>
> Dan bertakwalah pada Allah _tabâraka wa ta'âlâ_ 
>   dalam urusan dengannya.