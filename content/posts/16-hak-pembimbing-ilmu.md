Title: Hak 16: Hak pembimbingmu dalam ilmu
Date: 2023-12-28

    وأَمَّا حَقُّ سَائِسِكَ بالعِلْمِ التَّعْظِيمُ لَهُ والتَّوْقِيرُ لِمَجْلِسِهِ وَحُسْنُ الاسْتِمَاعِ إليهِ وَالإقْبَالُ عَلَيْه وأن لا ترفع عليه صوتك، ولا تجيب أحدا يسأله عن شئ حتى يكون هو الذي يجيب، ولا تحدث في مجلسه أحدا ولا تغتاب عنده أحدا وأن تدفع عنه إذا ذكر عندك بسوء وأن تستر عيوبه وتظهر مناقبه ولا تجالس له عدوا ولا تعادي له وليا فإذا فعلت ذلك شهدت لك ملائكة الله بأنك قصدته، وتعلمت علمه لله جل اسمه لا للناس.

> Dan hak pembimbingmu (_sa'is_) dalam ilmu,
>
> Kau takzim kepadanya, menghormati majlisnya,
>   menyimaknya dengan baik, menghadap padanya dengan sungguh-sungguh.
>
> Janganlah meninggikan suaramu kepadanya
> Janganlah menjawab orang yang bertanya kepadanya sesuatu, hingga ia yang menjawabya
> Janganlah mengobrol dengan siapapun saat sesi pelajaran darinya
> Janganlah menggosipkan keburukan seseorang kepadanya
>
> Jika ada orang yang berkata buruk tentangnya, maka kau harus membelanya
> Tutupilah keburukannya dan tampilkan keutamaannya
>
> Janganlah duduk bersamanya dengan permusuhan,
> Janganlah bermusuhan kepadanya dalam pertemanan,
>
> Jika engkau kerjakan semua, maka malaikat Allah akan bersaksi bagimu,
>   bahwa kau berredikasi kepadanya dan belajar ilmunya
>   untuk Allah SWT, bukan untuk manusia