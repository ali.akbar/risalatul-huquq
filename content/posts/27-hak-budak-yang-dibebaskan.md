Title: Hak 27: Hak Budak yang Dibebaskan
Date: 2023-12-31

    وأما حق مولاك الذي أنعمت عليه فأن تعلم أن الله عز وجل جعل عتقك له وسيلة إليه وحجابا لك من النار، وأن ثوابك في العاجل ميراثه، إذا لم يكن له رحم مكافأة بما أنفقت من مالك، وفى الأجل الجنة.

> Dan hak budak yang kau sukai (dengan membebaskannya),
>
> Kau perlu tahu,
>   Allah _azza wa jalla_ menjadikan pembebasan itu
>   perantaraanmu (_wasilah_) kepada-Nya,
>   dan penghalang bagimu dari neraka.
>
> Balasan langsungmu adalah kau menjadi ahli warisnya,
>   jika tidak ada keluarga kandungnya,
>   sebagai kompensasi dari nafkahmu untuk pembebasanya,
>   dan balasan utamanya adalah surga.