Title: Hak 14: Hak berkurban
Date: 2023-12-27

    وحق الهدي أن تريد به الله عز وجل ولا تريد به خلقه ولا تريد به إلا التعرض لرحمة الله ونجاة روحك يوم تلقاه.

> Dan hak berkurban (hadyu)
>
> Dengannya, kau inginkan Allah _Azza wa Jalla_,
>   tak kau inginkan makhluk-Nya,
> Tak kau inginkan dengannya, melainkan hanya
>   terbukanya jiwamu pada belas kasih Allah,
>   dan penyerahan ruhmu pada hari perjumpaan dengan-Nya