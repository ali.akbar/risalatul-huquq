Title: Hak 43: Hak Pemberi Nasihat
Date: 2023-12-31

    وحق الناصح أن تلين له جناحك وتصغي إليه بسمعك، فان أتى بالصواب حمدت الله عز وجل وإن لم وافق رحِمته ولم تتهمه وعلمت أنه أخطأ ولم تؤاخذه بذلك إلا أن يكون مستحقا للتهمة، فلا تعبأ بشيء من أمره على حال ولا قوة إلا بالله.

> Dan hak pemberi nasihat (_nâsih_),
>
> Kau berlaku lembut kepadanya,
>   mendengarkannya dengan perhatianmu.
>
> Jika ia memberimu nasihat yang akurat,
>   pujilah Allah _azza wa jalla_.
>
> Namun jika ia tidak setuju denganmu,
>   tampilkanlah kasih sayang kepadanya,
>   janganlah menuduhnya,
>
> Menurutmu ia salah,
>   namun jangan minta pertanggungjawabannya,
>   kecuali ia memang pantas dituduh.
>   setelahnya, jangan pedulikan lagi urusan ini.
>
> _wa lâ quwwata illâ billâh_