Title: Hak 33: Hak Sahabat
Date: 2023-12-31

The right of the partner (sharik) is that if he should be absent, you suffice him in his affairs, and if he should be present, you show regard for him. You make no decision without his decision and you do nothing on the basis of your own opinion, but you exchange views with him. You guard his property for him, and you do not betray him in that of his affair which is difficult or of little importance, for God's hand is above the hands of two partners as long as they do not betray each other. And there is no strength save in God.

    وأما حق الشريك فإنْ غابَ كَفَيتَهُ، وإن حضر رعيتَهُ، ولا تحكم دون حكمه، ولا تعمل برأيك دون مناظرته، وتحفظ عليه ماله، ولا تخونه فيما عز أو هان من أمره فإن يد الله تبارك وتعالى على أيدي الشريكين ما لم يتخاونا ولا قوة إلا بالله.

> Dan hak partner (_syarik_),
>
> Jika ia tidak ada maka kau melengkapi urusannya,
>   jika ia hadir, maka tunjukkan hormat padanya.
>
> Janganlah memutuskan tanpa putusannya,
>   janganlah melakukan sesuatu hanya dari pandanganmu,
>   tanpa tukar pandang dengannya.
>
> Jagakan hartanya baginya,
>   janganlah mengkhianatinya pada urusan yang sulit maupun remeh,
>
> Sesungguhnya tangan Allah _tabaraka wa ta'ala_ di atas tangan dua partner,
>   selama mereka tidak saling mengkhianati.
>
> _wa lâ quwwata illâ billâh_