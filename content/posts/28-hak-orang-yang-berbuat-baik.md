Title: Hak 28: Hak Orang yang Berbuat Baik
Date: 2023-12-31

    وَأمّا حَقُّ ذِي المَعْرُوفِ عَلَيكَ فَأَنْ تَشْكُرَهُ وتَذْكُرَ مَعْرُوفَهُ  ، وَتُخلِصَ لَهُ الدُّعَاءَ فِيمَا بَينَكَ وبَيْنَ اللَّهِ عز وجل ، فَإنّكَ إذَا فَعَلْتَ ذَلِكَ كُنْتَ قَدْ شَكَرْتَهُ سِرًّا وَعَلانِيَةً. ثُمَّ إنْ قدرت على مكافأته يوماً كافأته.

> Dan hak orang yang berbuat kebaikan (_dzul-ma'ruf_) kepadamu,
>
> Kau berterimakasih kepadanya dan ungkapkan kebaikannya
>   Kau menghadiahinya ucapan yang indah,
>   dan mendoakannya dengan ikhlas,
>   dengan doa antara dirimu dan Allah _azza wa jalla_;
>
> Jika kau telah lakukan itu,
>   berarti kau telah berterimakasih padanya,
>   secara sembunyi-sembunyi maupun diungkapkan.
>
> Jika suatu saat kau dapat membalasnya, maka balaslah ia