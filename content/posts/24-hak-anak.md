Title: Hak 24: Hak Anak
Date: 2023-12-31

    وَأمَّا حَقُّ وَلَدِكَ فَان تَعْلَمَ أنَّهُ مِنْكَ وَمُضَافٌ إلَيكَ فِي عَاجِلِ الدُنْيَا بخَيْرِهِ وَشَرِّهِ، وَأَنَّكَ مَسْئولٌ عَمَّا ولِّيتَهُ بِهِ مِنْ حُسْنِ الأَدَب وَالدّلالَةِ عَلَى رَبهِ وَالْمَعُونةِ لَهُ عَلَى طَاعَتِهِ فَاعْمَلْ فِي أَمْرِهِ عَمَلَ من يعلم أنه مثاب على الإحسان إليه مُعاقِب على الإساءةِ إليه. 

> Dan hak anakmu,
>
> Kau perlu tahu, ia darimu,
>   akan disambungkan kepadamu,
>   dalam urusan dunia yang singkat ini,
>   kebaikan dan keburukan dirinya.
>
> Kau akan ditanya tentang 
>   apa yang dititipkan padamu terkait dirinya:
>   keindahkan adabnya, arahan menuju Tuhannya,
>   dan membantunya untuk mematuhi-Nya.
>
> Maka dalam memerintahnya, berperilakulah
>   seperti orang yang tahu bahwa ia akan
>   dibalas atas kebaikan kepadanya
>   dan dihukum atas kejahatan kepadanya.