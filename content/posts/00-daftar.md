Title: Daftar

Hak Tuhan terhadap diri
01. Hak Tuhan
02. Hak Diri
03. Hak Lisan
04. Hak Pendengaran
05. Hak Penglihatan
06. Hak Tangan
07. Hak Kaki
08. Hak Perut
09. Hak Kemaluan

Hak Ibadah
10. Hak Shalat
11. Hak haji
12. Hak Puasa
13. Hak Sedekah
14. Hak Berkurban

Hak Pemipin
15. Hak Penguasa
16. Hak Pembimbing Ilmu
17. Hak Tuan Pemilikmu

Hak Pengikut
18. Hak Rakyat
19. Hak Pengikut Ilmu
20. Hak Istri
21. Hak Budak

Hak Saudara Kandung
22. Hak Ibu
23. Hak Ayah
24. Hak Anak
25. Hak Saudara

Hak Orang Lain
26. Hak Tuan yang Membebaskanmu
27. Hak Budak yang Dibebaskan
28. Hak Orang yang Berbuat Baik Padamu
29. Hak Muadzin
30. Hak Imam Shalat
31. Hak Teman Majlis
32. Hak Tetangga
33. Hak Sahabat
34. Hak Partner
35. Hak Harta
36. Hak Pemberi Kredit
37. Hak Kolega
38. Hak Lawan yang Mengklaim
39. Hak Lawan yang Diklaim
40. Hak Peminta Masukan
41. Hak Pemberi Masukan
42. Hak Peminta Petunjuk
43. Hak Pemberi Petunjuk
44. Hak yang Lebih Tua
45. Hak yang Lebih Muda
46. Hak yang Meminta
47. Hak yang Diminta
48. Hak Orang yang Membahagiakanmu
49. Hak Orang yang Berbuat Buruk Padamu
50. Hak Orang yang Sekepercayaan
51. Hak Ahlu Dzimmah


[ref]https://www.al-islam.org/treatise-rights-risalat-al-huquq-imam-ali-zayn-al-abidin/rights-allah-against-oneself[/ref]