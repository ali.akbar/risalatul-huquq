Title: Hak 13: Hak Sedekah
Date: 2023-12-27

    وَأَمَّا حَقُّ الصَّدَقَةِ فَأَنْ تَعْلَمَ أنَّها ذُخرُكَ عِنْدَ رَبكَ وَوَديعَتُكَ الَّتِي لا تَحْتَاجُ إلَى الإشْهَادِ عَليها، و كُنْتَ بمَا تَسْتَودِعُهُ سِرًّا أَوْثقَ مِنْكَ بمَا تَسْتَوْدِعُهُ عَلانِيَةً، و تعلم أنَّها تَدْفَعُ البلايا و الأسقام عنك في الدنيا، وتَدفَعُ عنك النار في الآخرة. 

> Dan hak sedekah,
> 
> Kau perlu tahu,
>   ia adalah simpananmu di sisi Tuhanmu,
>   titipanmu yang tak perlu saksi.
> Jika kau menyimpan ke dalamnya dengan kerahasiaan,
>   kau pelu lebih percaya itu daripada menyimpan ke dalamnya di hadapan publik.
>
> Kau perlu tahu, sedekah itu 
>   mencegah padamu cobaan dan penyakit di dunia
>   dan mencegah padamu neraka di akhirat