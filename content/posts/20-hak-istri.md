Title: Hak 20: Hak istri
Date: 2023-12-31

    وأما حق الزوجة فأن تعلم أن الله عز وجل جعلها لك سكنا وأنسا فتعلم أن ذلك نعمة من الله عليك فتكرمها وترفق بها، وإن كان حقك عليها أوجب فان لها عليك أن ترحمها لأنها أسيرك وتطعمها وتكسوها وإذا جهلت عفوت عنها. 

> Dan hak istrimu,
>
> Kau perlu tahu, Allah _azza wa jalla_ menjadikannya
>   ketenangan dan kenyamanan bagimu,
> Kau perlu tahu, itu adalah nikmat dari Allah bagimu,
> Maka muliakanlah ia, lemah-lembutlah dengannya,
> Hakmu atasnya diwajibkan (oleh Allah), maka sesungguhnya ia berhak
>   untuk kau perlakukan ia dengan penuh kasih sayang,
>   karena ia terbelenggu (_asîr_) dirimu.
> Berilah ia makan, pakaikanlah ia baju,
>   dan jika ia berlaku bodoh (_jahil_), maka maafkanlah ia.