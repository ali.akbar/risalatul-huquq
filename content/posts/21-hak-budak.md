Title: Hak 21: Hak budak
Date: 2023-12-31

    وأما حق مملوكك فأن تعلم أنه خلق ربك وابن أبيك وأمك ولحمك ودمك تملكه،لا أنت صنعته من دون الله ولا خلقت شيئا من جوارحه، ولا أخرجت له رزقا ولكن الله عز وجل كفاك ذلك ثم سخره لك وائتمنك عليه واستودعك إياه، ليحفظ لك ما تأتيه من خير إليه فأحسن إليه كما أحسن الله إليك، وإن كرهته استبدلت به، ولم تعذب خلق الله عز وجل ولا قوة إلا بالله.

> Dan hak budakmu (_mamlûk_)
>
> Kau perlu tahu, ia dicipakan Tuhanmu,
>   ia adalah anak ayah ibumu, ia darah dan dagingmu.
> Kau memilikinya, tapi kau tidak membuatnya; melainkan Allah.
>   kau tidak menciptakan suatupun dari anggota tubuhnya,
>   tidak pula mengeluarkan baginya rizki;
>   Allah-_azza wa jalla_-lah yang memberikan kecukupan padamu untuk itu.
>
> Kemudian, Ia tundukkan budak itu bagimu,
>   mengamanahkannya padamu, dan menitipkannya padamu
>   sehingga ia dapat menjagamu dengan pemberian baikmu padanya.
>
> Maka, berbuat baiklah kepadanya,
>   seperti Allah telah berbuat baik padamu.
> Dan jika kau tidak suka padanya, gantilah ia,
>   janganlah menyiksa ciptaan Allah _azza wa jalla_
>
> _wa lâ quwwata illâ billâh_