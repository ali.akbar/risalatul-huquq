Title: Hak 07: Hak Kaki
Date: 2023-07-09

    وَأَمَّا حَقُّ رِجْلَيْكَ فَأَنْ لا تَمْشِي بهِمَا إلَى مَا لا يَحِلُّ لَكَ فيهما، تقف على الصراط فانظر أن لا تزل بك فتتردى في النار.

> Dan hak kedua kakimu,
>
> Tidak berjalan dengan keduanya  
>   menuju arah yang tidak halal bagimu,  
> Berdirilah di atas jembatan (_ash-shirât_),
>   perhatikan, agar tidak terjatuh denganmu,
>   dan menyebabkanmu jatuh ke neraka.