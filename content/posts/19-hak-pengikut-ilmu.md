Title: Hak 19: Hak pengikut dengan kekuasaan
Date: 2023-12-31

    وأمَّا حَقُّ رَعِيَّتِكَ بالعِلْمِ فَأَنْ تَعْلَمَ أن الله عز وجل إنما جعلك قيما لهم فيما آتاك من العلم وفتح لك من خزائنه فان أحسنت في تعليم الناس، ولم تخرق بهم، ولم تضجر عليهم، زادك الله من فضله، وإن أنت منعت الناس علمك أو خرقت بهم عند طلبهم العلم منك كان حقا على الله عز وجل أن يسلبك العلم وبهاءه ويسقط من القلوب محلك. 

> Dan hak pengikutmu dalam ilmu,
>
> Kau perlu tahu, sesungguhnya hanya Allah-_azza wa jalla_-lah yang menjadikanmu pembimbing mereka,
>   melalui pemberian-Nya padamu dari Ilmu(-Nya),
>   Ia telah bukakan bagimu dari Khazanah-Nya
> Jika kau mengajari manusia dengan baik,
>   tidak merusak mereka, dan tidak bosan atas mereka,
>   maka Allah akan menambah bagimu dari Karunia-Nya.
> Tetapi jika kau menahan ilmumu dari manusia,
>   atau merusak mereka ketika belajar ilmu darimu,
>   maka hak Allah-_azza wa jalla_-lah untuk mencabut darimu ilmu dan kemegahannya
>   serta menjadikanmu jatuh dari posisimu pada hati manusia