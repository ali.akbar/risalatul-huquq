Title: Hak 18: Hak pengikut dengan kekuasaan
Date: 2023-12-31

    فأَمَّا حُقُوقُ رَعِيَّتِكَ بالسُّلْطَانِ فَأَنْ تَعْلَمَ أنهم صاروا رعيتك لضعفهم قوتك فيجب أن تعدل فيهم وتكون لهم كالوالد الرحيم، وتغفر لهم جهلهم، ولا تعاجلهم بالعقوبة، وتشكر الله عز وجل على ما آتاك من القوة عليهم.

> Dan hak pengikutmu dalam kekuasaan (rakyat),
>
> Kau harus tahu, mereka menjadi pengikutmu karena kelemahan mereka dan kekuatanmu
> Maka, wajib bagimu untuk adil terhadap mereka dan menjadi layaknya ayah yang penyayang
> Ampuni kejahilan mereka, janganlah buru-buru dengan hukuman
> Berterimakasihlah kepada Allah _azza wajalla_ atas karuniaNya, yaitu kekuasaan atas mereka