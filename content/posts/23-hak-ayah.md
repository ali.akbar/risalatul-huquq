Title: Hak 23: Hak Ayah
Date: 2023-12-31

    وَأمَّا حَقُّ أَبيكَ فان تَعْلَمَ أنَّهُ أَصْلُكَ، وَأَنَّهُ لَوْلاهُ لَمْ تَكُنْ. فَمَهْمَا رَأيْتَ فِي نفْسِكَ مِمَّا يُعْجِبُكَ فَاعْلَمْ أَنَّ أَبَاكَ أَصْلُ النِّعْمَةِ عَلَيْكَ فِيهِ وَاحْمَدِ اللَّهَ وَاشْكُرْهُ عَلَى قَدْرِ ذَلِكَ وَلا قُوَّةَ إلاّ باللهِ. 

> Dan hak ayahmu,
>
> Kau perlu tahu, ia adalah asalmu.
> Sesungguhnya, tanpanya kamu takkan ada.
> Maka kapanpun kau lihat pada dirimu sesuatu yang kau sukai,
>   ketahuilah, sesungguhnya ayahmu adalah asal muasal nikmat atasmu.
> Karena itu, pujilah Allah dan bersyukurlah kepadanya dalam ukuran itu.
>
> _wa lâ quwwata illâ billâh_