Title: Hak 45: Hak yang Muda
Date: 2024-01-01

    وحق الصغير رحمته في تعليمه والعفو عنه والستر عليه والرفق به والمعونة له. 

> Dan hak orang yang lebih muda (_saghîr_)
>
> Kau sayangi ia dalam
>    pengajarannya,
>    pemaafaan padanya,
>    perlindungan atasnya,
>    keramahan dengannya,
>    dan bantuan kepadanya.