Title: Hak 36: Hak Pemberi Kredit
Date: 2023-12-31

    وأما حق غريمك الذي يطالبك فإن كنت موسرا أعطيته وإن كنت معسرا أرضيته بحسن القول ورددته عن نفسك ردا لطيفا. 

> Hak pemberi pinjaman yang menagihmu,
>
> Jika kamu memiliki kecukupan,
>   maka tunaikanlah utangmu padanya.
> 
> Jika kamu masih dalam kesulitan,
>   maka buatlahi ia lega dengan perkataan yang baik,
>   dan tolaklah ia dengan kelembutan.