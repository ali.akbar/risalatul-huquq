Title: Hak 15: Hak penguasa
Date: 2023-12-28

    فأَمَّا حَقُّ السُّلْطَانِ فَأَنْ تَعْلَمَ أنّكَ جُعِلْتَ لَهُ فِتنَةً وأنَّهُ مُبْتَلىً فِيكَ بمَا جَعَلَهُ اللهُ لَهُ عَلَيْكَ مِنَ السُّلْطَانِ وأن عليك أن لا تتعرض لسخطه، فتلقي بيديك إلى التهلكة، وتكون شريكا له فيما يأتي إليك من سوء.

> Dan hak penguasa,
>
> Kau perlu tahu, Tuhan jadikan dirimu cobaan (fitnah) baginya
> Tuhan uji ia melalui kekuasaan yang dianugerahkan Allah kepada nya atas dirimu
> Jangan biarkan dirimu terekspos murkanya,
> karena dengannya kamu melemparkan dirimu sendiri ke dalam kehancuran,
> dan menjadi sekutunya saat ia menimpakan kejahatan bagimu