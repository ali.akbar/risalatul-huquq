Title: Hak 06: Hak Tangan
Date: 2023-07-14

    وَأَمَّا حَقُّ يَدِكَ فَأَنْ لا تَبْسُطَهَا إلَى مَا لا يَحِلُّ لَكَ. 

> Dan hak tanganmu,
>
> Kau tidak mengulurkan tanganmu  
>   ke yang tidak halal bagimu.