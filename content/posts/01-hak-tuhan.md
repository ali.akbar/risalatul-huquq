Title: Hak 01: Hak Terbesar Allah
Date: 2023-07-08

    فأَمَّا حَقُّ اللهِ الأَكْبَرُ فَإنَّكَ تَعْبُدُهُ لا تُشْرِكُ بهِ شَيْئاً، فَإذَا فَعَلْتَ ذَلِكَ بإخلاصٍ جَعَلَ لَكَ عَلَى نَفْسِهِ أَنْ يَكفِيَكَ أَمْرَ الدُّنْيَا وَالآخِرَةِ وَيَحْفَظَ لَكَ مَا تُحِبُّ مِنْهما.

> Maka Hak terbesar Allah adalah engkau menyembah-Nya tanpa menyekutukan sesuatupun dengan-Nya.
>
>Jika engkau lakukan itu dengan ikhlas,  
>Ia jadikan atas diri-Nya bagimu kecukupanmu  
>pada urusan dunia dan akhirat.
>
>Serta Ia pelihara bagimu apa yang engkau sukai  
> dari keduanya (dunia dan akhirat).