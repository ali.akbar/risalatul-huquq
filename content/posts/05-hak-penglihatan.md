Title: Hak 05: Hak Penglihatan
Date: 2023-07-08

    وَأَمَّا حَقُّ بَصَرِكَ أن تُغْمِضَهُ عَمَّا لا يَحِلُّ لَكَ و تَعْتَبِر بالنَّظَرِ بِهِ. 

> Dan hak penglihatanmu,
>
> Engkau tundukkan ia dari
>   dari segala sesuatu yang tidak halal bagimu,  
> dan kau pertimbangkan apapun yang kau lihat dengannya.