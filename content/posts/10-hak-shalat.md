Title: Hak 10: Hak Shalat
Date: 2023-12-21

    فَأَمَّا حَقُّ الصَّلاةِ فَأَنْ تَعْلَمَ أنّهَا وِفَادَةٌ إلَى اللهِ وَأَنَّكَ فيها قَائِمٌ بَيْنَ يَدَيِ اللهِ، فَإذَا عَلِمْتَ ذَلِكَ قُمْتَ مَقَامَ الذَّلِيلِ الْحَقيرِ الرَّاغِب الرَّاهِب الرَّاجِي الْخَائِفِ الْمِسْتَكِينِ الْمُتَضَرِّعِ الْمُعَظِّمِ لِمَنْ كانَ بَيْنَ يَدَيْهِ بالسُّكُونِ و الوقارِ و تقبل عليها بِقَلبِكَ و تقيمها بِحُدُودِها و حُقوقِها

> Dan hak shalat,
>
> Kau perlu tahu, ia adalah serambi menuju Allah,
> sungguh padanya kau berdiri di hadapan Allah.
>
> Maka, jika kau sudah tahu,
> kau akan berdiri bagaikan orang yang
> rendah, peminta, pemohohon,
> gemetar, penuh harap, takut dan hina.
> 
> Mengagungkan Ia yang berada di hadapanmu
> dengan ketenangan dan khidmat,
> Mendatangi salat dengan hatimu
> dan mendirikan salat sesuai batasan dan haknya.